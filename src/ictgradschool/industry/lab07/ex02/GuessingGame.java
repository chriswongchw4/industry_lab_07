package ictgradschool.industry.lab07.ex02;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    /**
     * Plays the actual guessing game.
     * <p>
     * You shouldn't need to edit this method for this exercise.
     */
    public void start() {

        int number = getRandomValue();
        int guess = 0;

        while (guess != number) {

            guess = getUserGuess();


            if (guess > number) {
                System.out.println("Too high!");
            } else if (guess < number) {
                System.out.println("Too low!");
            } else {
                System.out.println("Perfect!");
            }

        }

    }

    /**
     * Gets a random integer between 1 and 100.
     * <p>
     * You shouldn't need to edit this method for this exercise.
     */
    private int getRandomValue() {
        return (int) (Math.random() * 100) + 1;
    }

    /**
     * Gets the user's guess from the keyboard. Currently assumes that the user will always enter a valid guess.
     * <p>
     * TODO Implement some error handling, for the cases where the user enters a value that's too big, too small, or
     * TODO not an integer. Change this method so it's guaranteed to return an integer between 1 & 100, inclusive.
     */
    private int getUserGuess() {
        int x = 0;

        while (!(x >= 1 && x <= 100)) {
            try {
                System.out.print("Enter a number: ");
                x = Integer.parseInt(Keyboard.readInput());

            } catch (NumberFormatException e) {
                System.out.println("Error. Please enter a number.");
            }

            while (x < 0 || x > 100){
                try {
                    System.out.print("Error. Please enter a number between 1-100(inclusive):");
                    x = Integer.parseInt(Keyboard.readInput());

                } catch (NumberFormatException e) {
                    System.out.println("Error. Please enter a number.");
                }
            }
        }

        return x;
    }


    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
