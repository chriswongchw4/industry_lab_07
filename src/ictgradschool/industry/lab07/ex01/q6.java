package ictgradschool.industry.lab07.ex01;


public class q6 {



    private void tryCatch06() {
        try {
            try06(0, "");
            System.out.println("A");
        } catch (ArithmeticException e) {
            System.out.println("B Error");
        }
    }

    private void try06(int num, String s) {
        System.out.println("C");
        try {
            num = s.length();
            num = 200 / num;
        } catch (NullPointerException e) {
            System.out.println("E Error");
        }
        System.out.println("F");
    }



    private void tryCatch08() {
        try {
            try08(0, null);
            System.out.println("A");
        } catch (NullPointerException e) {
            System.out.println("B");
        }
    }

    private void try08(int num, String s) {
        System.out.println("C");
        try {
            num = s.length();
            System.out.println("D");
        } finally {
            System.out.println("E");
        }
        System.out.println("F");
    }



    private void throwsClause09() {
        try {
            throws09(null);
            System.out.println("A");
        } catch (NullPointerException e) {
            System.out.println(e);
        }
        System.out.println("B");
    }

    private void throws09(String numS) throws NullPointerException {
        if (numS == null) {
            throw new NullPointerException("Null String");
        }
        System.out.println("C");
    }



    private void throwsClause10() {
        try {
            throws10(null);
            System.out.println("A");
        } catch (ArithmeticException e) {
            System.out.println(e);
        } finally {
            System.out.println("B");
        }
        System.out.println("C");
    }

    private void throws10(String numS) throws NullPointerException {
        if (numS == null) {
            throw new NullPointerException("Bad String");
        }
        System.out.println("D");
    }


    public static void main(String[] args) {
        q6 q6 = new q6();
    }
}
