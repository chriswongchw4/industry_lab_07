package ictgradschool.industry.lab07.ex06;

import ictgradschool.Keyboard;

import java.util.ArrayList;

/**
 * TODO Write a program according to the Exercise Six guidelines in your lab handout.
 */
public class ExerciseSix {

    public void start() {
        // TODO Write the codes :)

        System.out.print("Enter a string of at most 100 characters: ");
        try {
            String beforeedit = getUserInput();
            String[] splitWords = seperateWords(beforeedit);
            String[] validWords = checkValidWords(splitWords);

            System.out.print("You entered: ");
            for (int x = 0; x < validWords.length; x++) {
                System.out.print(validWords[x].charAt(0) + " ");
            }


        } catch (ExceedMaxStringLengthException e) {
            System.out.println(e.getMessage());
        } catch (InvalidWordException e) {
            System.out.println(e.getMessage());
        }


    }


    // TODO Write some methods to help you.


    public String[] checkValidWords(String[] splitWords) throws InvalidWordException {

        ArrayList<String> templist = new ArrayList<>();


        for (int x = 0; x < splitWords.length; x++) {
            if (splitWords[x].substring(0, 1).matches("[0-9]")) {
                throw new InvalidWordException(splitWords[x] + " is not a words.");
            } else {
                templist.add(splitWords[x]);

            }

        }

        String[] validwords = new String[templist.size()];

        for (int x = 0; x < templist.size(); x++) {
            validwords[x] = templist.get(x);
        }

        return validwords;
    }

    public String[] seperateWords(String beforeedit) {

        String[] splitWords = beforeedit.split(" ");
        return splitWords;
    }


    public String getUserInput() throws ExceedMaxStringLengthException {
        String userInput;
        userInput = Keyboard.readInput();
        if (userInput.length() > 100) {
            throw new ExceedMaxStringLengthException("There are more than 100 characters.");
        }

        return userInput;
    }


    public class ExceedMaxStringLengthException extends Exception {
        public ExceedMaxStringLengthException() {}

        public ExceedMaxStringLengthException(String message) {super(message);}

        public ExceedMaxStringLengthException(Throwable t) {super(t);}

        public ExceedMaxStringLengthException(String message, Throwable t) {super(message, t);}

    }


    public class InvalidWordException extends Exception {
        public InvalidWordException() {}

        public InvalidWordException(String message) {super(message);}

        public InvalidWordException(Throwable t) {super(t);}

        public InvalidWordException(String message, Throwable t) {super(message, t);}

    }


    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseSix().start();
    }
}
