package ictgradschool.industry.lab07.ex04;

import ictgradschool.Keyboard;

public class ExerciseFour {
    /**
     * Main program entry point. Do not edit, except for uncommenting the marked lines when required.
     */
    public static void main(String[] args) {
        ExerciseFour program = new ExerciseFour();

        // Exercise four, Question 1
        program.divideNumbers();

        // Exercise four, Question 2
        // TODO You may uncomment this line to help test your solution to question two.
        // program.question2();

        // Exercise four, Question 3
        // TODO You may uncomment this line to help test your solution to question three.
        // program.question3();
    }

    /**
     * The following tries to divide using two user input numbers, but is prone to error.
     * <p>
     * TODO Add some error handling to the code as follows:
     * TODO 1) If the user enters 0 for the second number, "Divide by 0!" should be printed instead of crashing the
     * program.
     * TODO 2) If the user enters numbers which aren't integers, "Input error!" should be printed instead of crashing
     * the program.
     * TODO Both these exceptional cases should be handled in the same try-catch block.
     */

    public int getUserinput() {
        int num = 0;

        while (num == 0) {
            try {
                String str1 = Keyboard.readInput();
                num = Integer.parseInt(str1);
                if (num == 0) {
                    break;
                }
            } catch (NumberFormatException e) {
                System.out.print("Input error!Please enter again: ");
            }
        }
        return num;
    }


    public void divideNumbers() {

        int num1;
        int num2 = 0;


        while (num2 == 0) {
            try {
                System.out.print("Enter the first number: ");
                num1 = getUserinput();
                System.out.print("Enter the second number: ");
                num2 = getUserinput();

                System.out.println(num1 + " / " + num2 + " = " + (num1 / num2));


            } catch (ArithmeticException e) {
                System.out.println("Divide by 0! Please enter again.");
            }
        }


        // Output the result


    }
/*
    public void question2() throws StringIndexOutOfBoundsException {
        if (int x<0){
            throw new StringIndexOutOfBoundsException("Wrong string index!");
        }
        //TODO Write some Java code which throws a StringIndexOutOfBoundsException
    }

    public void question3() throws ArrayIndexOutOfBoundsException {
        if (int x<0){
            throw new ArrayIndexOutOfBoundsException("Wrong Array index!");
        }

        //TODO Write some Java code which throws a ArrayIndexOutOfBoundsException
    }*/
}